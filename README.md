# Piped Instances Fetcher

This is a little script to generate a json index from Piped's Instances wiki.

# Usage

You just need to run `generate_index.js` through node:

```node
node generate_index.js
```

This will generate a file called `index.json`.
