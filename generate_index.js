const fs = require("fs");

const checkAvailability = async (instance) => {
  try {
    const headers = new Headers();

    headers.append("User-Agent", "piped-instances-fetcher/1.0");

    const response = await fetch(instance.apiUrl, { headers, method: "HEAD" });


    if (response.ok && response.status !== 521) {
      instance.frontendUrl = new URL(response.url).origin;

      instance.isAvailable = true;
    } else {
      instance.isAvailable = false;
    }
  } catch (e) {
    instance.isAvailable = false;
  }

  console.info(`fetched ${instance.apiUrl.origin}`);
  return instance;
};

const generateInstancesList = async () => {
  console.info("Generating index...")
  var instances = [];

  console.info("Fetching instances wiki...")
  const body = await (
    await fetch(
      "https://raw.githubusercontent.com/wiki/TeamPiped/Piped-Frontend/Instances.md"
    )
  ).text();

  var skipped = 0;

  // Each instance is in its own line
  const lines = body.split("\n");

  console.info("Fetching all urls...")
  for (const line of lines) {
    const split = line.split("|");
    if (split.length === 5) {
      // Skip first lines which are not instances
      if (skipped < 2) {
        skipped++;
        continue;
      }

      const instance = {};

      instance.name = split[0].trim();
      instance.apiUrl = new URL(split[1].trim());
      instance.location = split[2]
        .trim()
        .split(",")
        .map((location) => location.trim());
      instance.cdn = split[3].trim();
      await checkAvailability(instance);

      instances.push(instance);

    }
  }
  return instances;
};

const run = async () => {
  try {
    const instances = await generateInstancesList();

    if (instances.length > 0) {
      console.info("Writing instances in index.json...");
      fs.writeFile(__dirname + "/index.json", JSON.stringify(instances), (err) => {
        if (err) {
          throw err;
        } else {
          console.info("Index updated");
        }
      });
    }
  } catch (e) {
    console.error(e);
  }
};

run();
